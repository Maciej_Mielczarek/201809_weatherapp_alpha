To run:
0. Open a Console/Terminal
1. cd to the project's root directory ("Weather")
2. Type **gradle wrapper** in the Console/Terminal
3. Type **./gradlew bootrun** in the Console/Terminal
To use (test):
0. Run the project
1. Make sure an sql server with a copy of my database is running locally. Note: I'm pretty sure that's not how the whole 'connecting to an sql server with logs' is supposed to work
2. Log in to the database (called **weather**) with user **myuser** and password **xxxx** and select the whole table **log** to view its current contents
3. Open an internet browser and go to **http://localhost:8080/cities/Szczecin/days/5**. You can replace **Szczecin** with a different existing city, but for now (in this test version) stick with **5 days**
4. You should now see a json with information about average temperature in 5 days. No other information in this test version
5. Notice the changes in table **log** of database **weather**