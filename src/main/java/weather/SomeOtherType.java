package weather;

import java.util.Map;
import com.fasterxml.jackson.annotation.*;

public class SomeOtherType {
    private Location location;
    private Current current;
    private Forecast forecast;

    @JsonProperty("location")
    public Location getLocation() { return location; }
    @JsonProperty("location")
    public void setLocation(Location value) { this.location = value; }

    @JsonProperty("current")
    public Current getCurrent() { return current; }
    @JsonProperty("current")
    public void setCurrent(Current value) { this.current = value; }

    @JsonProperty("forecast")
    public Forecast getForecast() { return forecast; }
    @JsonProperty("forecast")
    public void setForecast(Forecast value) { this.forecast = value; }

    @Override
    public String toString() {
        return "SomeOtherType{" +
                "location=" + location +
                ", current=" + current +
                ", forecast=" + forecast +
                '}';
    }
}
