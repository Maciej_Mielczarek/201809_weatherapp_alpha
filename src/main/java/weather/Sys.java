package weather;

import java.util.Map;
import com.fasterxml.jackson.annotation.*;

public class Sys {
    private String pod;

    @JsonProperty("pod")
    public String getPod() { return pod; }
    @JsonProperty("pod")
    public void setPod(String value) { this.pod = value; }

    @Override
    public String toString() {
        return "Sys{" +
                "pod='" + pod + '\'' +
                '}';
    }
}
