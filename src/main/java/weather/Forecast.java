package weather;

import java.util.Arrays;
import java.util.Map;
import com.fasterxml.jackson.annotation.*;

public class Forecast {
    private Forecastday[] forecastday;

    @JsonProperty("forecastday")
    public Forecastday[] getForecastday() { return forecastday; }
    @JsonProperty("forecastday")
    public void setForecastday(Forecastday[] value) { this.forecastday = value; }

    @Override
    public String toString() {
        return "Forecast{" +
                "forecastday=" + Arrays.toString(forecastday) +
                '}';
    }
}
