package weather;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.sql.*;

import java.util.ArrayList;

@RestController
public class Cities {
    private ArrayList<City> AllCities;

    public Cities() {
        AllCities = new ArrayList<>();
    }

    public ArrayList<City> getAllCities() {
        return AllCities;
    }

    public void setAllCities(ArrayList<City> allCities) {
        AllCities = allCities;
    }

    @GetMapping("/cities/{name}/days/{days}")
    String tellWeather(@PathVariable String name, @PathVariable int days) {
        RestTemplate restTemplate = new RestTemplate();

        FiveDWeatherType wdt = restTemplate.getForObject("http://api.openweathermap.org/data/2.5/forecast?q=" + name + "&appid=e98ec0b55eff87bed3170a3ba71bd2b5", FiveDWeatherType.class);
        SomeOtherType sdt = restTemplate.getForObject("http://api.apixu.com/v1/forecast.json?key=74debbafc7f84884840122104182809&q=" + name + "&days=" + days, SomeOtherType.class);
        long now = System.currentTimeMillis() / 1000L;
        int msnow = (int)(System.currentTimeMillis() % 1000L);
        double temp1=0, temp2=0;
        int temp1cnt=0, temp1measurements = (int) wdt.getCnt(), checked=0;
        for(;checked<temp1measurements;++checked)
        {
            if(wdt.getList()[checked].getDt()-now >= 3600*24*5-3600*12 && wdt.getList()[checked].getDt()-now < 3600*12+3600*24*5)
            {
                temp1+=wdt.getList()[checked].getMain().getTemp()-273.15;
                ++temp1cnt;
            }
        }
        temp1 /= temp1cnt;
        temp2 = sdt.getForecast().getForecastday()[4].getDay().getAvgtempC();
        double tempavg = (temp1+temp2)/2;
        String Query = "\"/cities/"+name+"/days/"+days+"\"";
        String Result = "\"WeatherForecast{temperature="+tempavg+"}\"";
        try
                (
                        Connection conn = DriverManager.getConnection(
                                "jdbc:mysql://localhost:3306/weather?useSSL=false&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC","myuser","xxxx");
                        Statement stmt = conn.createStatement()
                )
        {
            String logEntry = "INSERT into log values ("+(int)now+", "+msnow+", "+Query+", "+Result+");";
            stmt.execute(logEntry);
        }
        catch(SQLException ex) {
            ex.printStackTrace();
        }

        //return "This function will be checking weather in city " + name + ", " + days + " days from now.\n\n" /*+ wdt + "\n\n" + sdt */+
        //        "avg temp in 5 days: "+ (temp1+temp2)/2;
        return Result;
    }


    /*@GetMapping("/cities")
    Cities listCities()
    {
        Cities ret = new Cities();
        try
                (
                        Connection conn = DriverManager.getConnection(
                                "jdbc:mysql://localhost:3306/weather?useSSL=false&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC","myuser","xxxx");
                        Statement stmt = conn.createStatement()
                        )
        {
            String strSelect = "SELECT * FROM cities";

            ResultSet rset = stmt.executeQuery(strSelect);
            while(rset.next()) {
                ret.AllCities.add(new City(rset.getInt("id"),rset.getString("name"),rset.getDouble("lat"),rset.getDouble("lon")));
            }
        }
        catch(SQLException ex) {
            ex.printStackTrace();
        }


        return ret;
    }
    @PostMapping("/cities")
    void createCityList(String sourceAddress)
    {
        try
            (
                    Connection conn = DriverManager.getConnection(
                            "jdbc:mysql://localhost:3306/weather?useSSL=false&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC","myuser","xxxx");
                    Statement stmt = conn.createStatement()
            )
        {
            String strCreateTable = "create table if not exists cities";
            String Col1 = "id int";
            String Col2 = "name varchar(45)";
            String Col3 = "lat double";
            String Col4 = "lon double";
            ResultSet rset = stmt.executeQuery(strCreateTable+"("+Col1+", "+Col2+", "+Col3+", "+Col4+")");
        }
        catch(SQLException ex) {
            ex.printStackTrace();
        }
    }*/





}
